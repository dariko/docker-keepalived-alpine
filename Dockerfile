FROM alpine:3.12.0

RUN apk add --no-cache \
        bash \
        curl \
        ipvsadm \
        iproute2 \
        keepalived

ADD run.sh /run.sh

CMD ["/run.sh"]
