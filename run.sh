#!/bin/bash -x

rm -f /tmp/keepalived.pid

exec /usr/sbin/keepalived \
         --dont-fork \
         --log-console \
         --use-file /etc/keepalived.conf \
         --pid /tmp/keepalived.pid
